//
//  NetworkManager.swift
//  fetcherimmagini
//
//  Created by Salvatore Penitente on 14/04/2017.
//  Copyright © 2017 Salvatore Penitente. All rights reserved.
//

import Foundation
import UIKit

enum NetworkError
{
    case invalidURL (String)
    case networkingError
    case permissionDenied
}

class NetworkManager
{
    static let shared = NetworkManager()
    
    func imageFetch (_ imageURLAsString: String, completion: @escaping (_ imageDownloaded: UIImage?, _ downloadingError: Error?) -> Void)
    {
        let urlFromString = URL(string: imageURLAsString)
        let imageDWURLRequest = URLRequest(url: urlFromString!)
        
        let imageDWSession = URLSession.shared.dataTask(with: imageDWURLRequest) {(data, response, error) in
            if (error != nil)
            {
                print("Errore nella creazione della URLSession, con errore \(String(describing: error))")
                DispatchQueue.main.async {
                    completion(nil, error)
                }
                return
            }
            else
            {
                if let imageDownloaded = UIImage(data: data!)
                {
                    DispatchQueue.main.async {
                        completion(imageDownloaded, nil)
                    }
                }
            }
        }
        imageDWSession.resume()
    }
}
